[![Python27](https://img.shields.io/badge/python-2.7-blue.svg)](https://www.python.org/download/releases/2.7/) [![Open Source Love](https://badges.frapsoft.com/os/mit/mit.svg?v=102)](https://github.com/ellerbrock/open-source-badge/)

# Project Goal

We have some customer records in a text file (customers.json) -- one customer per line, JSON-encoded. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).


## Installation

It's recommended to create new virtualenv for the project, then install all the requirements as follows:

```
cd InviteBot
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

## Getting output

There is a script in `./bin` folder which you'll need to run to get the output file, both printed in console and as text file. Make sure that you have the input file (here) [gistfile1.txt](gistfile1.txt) in the root folder

```
cd InviteBot
bin/bot
```

Existing output file will be overridden with each execution.


## Skeleton

```bash
├── bin
│    ├── bot
├── bot
│    ├── __init__.py
│    ├── customer.py
│    ├── helpers.py
│    ├── main.py
├── tests
│    ├── __init__.py
│    ├── customer_tests.py
│    ├── helpers_tests.py
│    ├── main_tests.py
├── .gitignore
├── LICENSE.md
├── MANIFEST.in
├── Makefile
├── README.md
├── gistfile1.txt
├── output.txt
├── requirements.txt
├── setup.py
```


## Tests

You can run all the tests using:

```
$ nosetests -v
```


## Authors

* **Saurabh B**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

