# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE.md') as f:
    license = f.read()

setup(
    name='bot',
    version='0.1',
    description='Invite bot outputs all the customer located at 100kms for Intercom Dublinoffice',
    long_description=readme,
    author='Saurabh B',
    author_email='saurabhbansod1992@gmail.com',
    license=license,
    packages=find_packages(exclude=('tests'))
)

