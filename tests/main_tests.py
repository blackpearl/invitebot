import os
import sys
sys.path.append("..")

from nose.tools import *
from bot.main import *

class TestMain(object):

    def setup(self):
        """Remove output file if it already exists"""
        try:
            os.remove("./output.txt")
        except Exception as e:
            pass

    def teardown(self):
        print "Nothing to do here!"

    def test_select_customer(self):
        """Check if main function is writing the output file"""
        select_customers()
        assert os.path.exists("./output.txt") == 1
