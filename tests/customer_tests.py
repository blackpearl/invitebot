import sys
sys.path.append("..")

from nose.tools import *
from bot.customer import *


class TestCustomer(object):

    def setup(self):
        self.lat = 53.2451022
        self.lon = -6.238335

    def teardown(self):
        print "Nothing to do here!"

    def test_is_invited(self):
        """Should return True for co-ordinates at distance greater than 100km
        from Dublin office, False otherwise.
        """
        customer = Customer("1", "foobar", self.lat, self.lon)

        assert customer.is_invited() == True

        """Changing co-ordinates to distance of more than 100km from Dubling
        office
        """
        customer.lat, customer.lon = 53.4692815, -9.436036
        assert customer.is_invited() == False

    def test_set_max_distance(self):
        """Test for updating class variable
        """
        Customer.set_max_distance(1000)
        assert Customer.MAX_DISTANCE == 1000
