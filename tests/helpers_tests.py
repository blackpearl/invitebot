import os
import sys
sys.path.append("..")

from nose.tools import *
from bot.helpers import *
import geopy.distance

class TestHelpers(object):

    def setup(self):
        """Setup office co-ordinates"""
        self.lat = 53.339428
        self.lon = -6.257664

        """Remove output file if it already exists"""
        try:
            os.remove("./output.txt")
        except Exception as e:
            pass

    def teardown(self):
        print "Nothing to do here!"

    def test_calculate_distance(self):
        """Check if distance calculated by helper method matches
        close to that of calculated with geocoder
        """
        coords_1 = (self.lat, self.lon)
        coords_2 = (53.4692815, -9.436036)

        assert int(calculate_distance(coords_2[0], coords_2[1])) == \
            int(geopy.distance.vincenty(coords_1, coords_2).km)

    def test_write_output(self):
        write_output("1", "foobar")
        with open('./output.txt', 'r') as f:
            first_line = f.readline()

        assert first_line == "1 foobar\n"

    def test_sort_output(self):
        """Add two lines to the output file, line user id being
        first
        """
        write_output("2", "foobar2")
        write_output("1", "foobar1")
        sort_output()
        with open('./output.txt', 'r') as f:
            first_line = f.readline()

        assert first_line == "1 foobar1\n"

    @raises(IOError)
    def test_print_output_error(self):
        """Remove output file and check if print ouput raise an
        I/O error
        """
        self.setup()
        print_output()
