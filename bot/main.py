# -*- coding: utf-8 -*-
import sys
sys.path.append(".")

import json
import os
from customer import Customer
from helpers  import write_output, print_output


"""Select customers within 100km from dublin office, note that with current
setup we can easily make maximum distance and file name to be passed by user
"""
def select_customers():

    try:
        customer_data = os.path.abspath('./gistfile1.txt')

        """Reading one line at a time as reading everything at once
        could be problematic for large sized file
        """
        with open(customer_data) as infile:
            for line in infile:
                data = json.loads(line)

                """Use Customer class to encapsulate each customer's
                data points. Check if customer can be invited and write
                output to the file

                Using nested block here in order to skip lines that can be
                processed due to some error, this will help us to get results
                for lines with proper data
                """
                try:
                    customer = Customer(data['user_id'], data['name'], \
                        float(data['latitude']), float(data['longitude']))
                    if customer.is_invited(): write_output(data['user_id'], \
                        data['name'])
                except ValueError as err:
                    print("ValueError: {0}".format(err))
                except KeyError as err:
                    print("Missing: {0} for a customer".format(err))
                except:
                    print("Unexpected error:", sys.exc_info()[0])

            print_output()

    except IOError as err:
        print("I/O error: {0}, maybe try running the project using bin/bot ?".format(err))
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

if __name__ == '__main__':
    select_customers()
