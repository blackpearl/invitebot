from math import radians, cos, sin, asin, sqrt

"""Calculate the great circle distance between two points
on the earth (specified in decimal degrees)
"""
def calculate_distance(lat, lon):
    EARTH_RADIUS, OFFICE_LAT, OFFICE_LON = 6371, 53.339428, -6.257664

    # convert decimal degrees to radians
    OFFICE_LON, OFFICE_LAT, lon, lat = map(radians, [OFFICE_LON, \
        OFFICE_LAT, lon, lat])

    # Great-circle distance formula
    dlon = lon - OFFICE_LON
    dlat = lat - OFFICE_LAT
    a = sin(dlat/2)**2 + cos(OFFICE_LAT) * cos(lat) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    return c * EARTH_RADIUS

"""Write a output file containing name and user id for invited
guests
"""
def write_output(user_id, name):
    with open('./output.txt','a+') as f:
        f.write("{} {}\n".format(user_id, name))

"""Output needs to be sorted by used ids in ascending order"""
def sort_output():
    with open("./output.txt", "r+") as f:
        lines = sorted(f.readlines(), key=lambda user_id: int(user_id.split()[0]))
        print ("\nTotal number of customers invited to the party: {}, the "
        "list is as follows =>\n".format(len(lines)))
        f.seek(0)
        f.writelines(lines)

"""Print contents of the output file"""
def print_output():
    try:
        sort_output()
        output_file = open('./output.txt', 'rb')
        output = output_file.read()
        print(output)
        output_file.close()
    except IOError as err:
        print("I/O error: {0}".format(err))
        raise
