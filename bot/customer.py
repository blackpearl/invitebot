# -*- coding: utf-8 -*-
import sys
sys.path.append(".")

from helpers import calculate_distance

class Customer(object):

    """Max distance for invitee from dublin office"""
    MAX_DISTANCE = 100

    def __init__(self, user_id, name, lat, lon):
        self.user_id = user_id
        self.name = name
        self.lat = lat
        self.lon = lon

    """A customer is invited if they are located within 100kms of dublin office"""
    def is_invited(self):
        return True if calculate_distance(self.lat, self.lon) <= self.MAX_DISTANCE \
            else False

    """Use in case you want to modify max distance"""
    @classmethod
    def set_max_distance(cls, distance):
        cls.MAX_DISTANCE = distance

    def __repr__(self):
        return "'{}'('{}','{}','{}')".format(self.__class__.__name__, self.user_id, \
            self.lat, self.lon)
    def __str__(self):
        return 'Customer Details:\n User ID: {}\n Lat: {}\n Lan: {}\n'.format(self.user_id, \
            self.lat, self.lon)
